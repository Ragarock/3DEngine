cmake_minimum_required(VERSION 3.0)

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "-std=c++14")
set(CPPFLAGS "${INC_FLAGS} -g -MMD -MP")
set(CPPFLAGS "$(shell pkg-config --cflags freetype2)")

project(game)
FILE(COPY res/ DESTINATION "${CMAKE_BINARY_DIR}/res/")

include_directories(
	/usr/include/freetype2
)

add_executable(
	game
	src/Main.cpp
	src/Camera.cpp
	src/Font.cpp
	src/Transform.cpp
	src/IndexBuffer.cpp
	src/Input.cpp
	src/Log.cpp
	src/Renderer.cpp
	src/Shader.cpp
	src/Texture.cpp
	src/VertexArray.cpp
	src/VertexBuffer.cpp
	src/Window.cpp
	src/Mesh.cpp
	src/Memory.cpp
	src/GenericMemory.cpp
	src/cmwc4096.c
	src/ecs/ecsComponent.cpp
	src/ecs/ecsSystem.cpp
	src/ecs/ecs.cpp
	src/Vendor/stb_image.cpp
)

target_link_libraries(
	game
	GL
	GLEW
	glfw
	freetype
)
