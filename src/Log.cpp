#include "Log.hpp"

int Log::m_logLevel = logLevelError;

void Log::err(const char* msg) {
	if(m_logLevel >= logLevelError)
		std::cout << "[ERR]: " << msg << std::endl;
}

void Log::warn(const char* msg) {
	if(m_logLevel >= logLevelWarning)
		std::cout << "[WARN]: " << msg << std::endl;
}

void Log::info(const char* msg) {
	if(m_logLevel >= logLevelInfo)
		std::cout << "[INFO]: " << msg << std::endl;
}
