#pragma once

#include <iostream>

#include <vector>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Input.hpp"

class Camera {
	private:
		float speed = 5;
		float sensitivity = .25;
		float fov = 90;

		glm::vec3 up;
		glm::vec3 forward;
		glm::vec3 right;

		void updateVectors();

	public:
		float yaw = 0;
		float pitch = 0;
		glm::vec3 position;
		Camera(glm::vec3 position = glm::vec3(0, 0, 0), glm::vec3 up = glm::vec3(0, 1, 0), glm::vec3 forward = glm::vec3(0, 0, 1));
		void update(float delta);

		glm::mat4 getView() {
			return glm::lookAt(this->position, this->position + this->forward, this->up);
		}

		glm::mat4 getSkyboxView() {
			return glm::lookAt(glm::vec3(0, 0, 0), this->forward, this->up);
		}

};
