#include "Renderer.hpp"

#include <iostream>

void GLClearError() {
	while(glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while(GLenum error = glGetError()) {
		std::cout << "[OpenGL Error](" << error << "): " << function << " " << file << ":" << line << std::endl;
		return false;
	}
	return true;
}

void Renderer::draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader, bool debug) const {
	shader.bind();
	va.bind();
	ib.bind();

	if(debug) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	GLCall(glDrawElements(GL_TRIANGLES, ib.getCount(), GL_UNSIGNED_INT, nullptr));

	if(debug) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void Renderer::clear() const {
	GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::setCamera(Camera* camera) {
	this->camera = camera;
}
