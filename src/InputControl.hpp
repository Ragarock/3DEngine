#pragma once

#include "Maths.hpp"

class InputControl {
	private:
		float amt;

	public:
		InputControl();

		void addAmt(float amtToAdd);
		float getAmt();
};

inline InputControl::InputControl() : amt(0.0f) {}

inline void InputControl::addAmt(float amtToAdd) {
	amt += amtToAdd;
}

inline float InputControl::getAmt() {
	return Maths::clamp(amt, -1.0f, 1.0f);
}
