#pragma once

#include <GL/glew.h>
#include <signal.h>

#include "Shader.hpp"
#include "Camera.hpp"
#include "VertexArray.hpp"
#include "IndexBuffer.hpp"

#define ASSERT(x) if (!(x)) raise(SIGTRAP);
#define GLCall(x) GLClearError();\
	x;\
	ASSERT(GLLogCall(#x, __FILE__, __LINE__))

void GLClearError();
bool GLLogCall(const char* function, const char* file, int line);

class Shader;

class Renderer {
	private:
		Camera* camera;

	public:
		void clear() const;
		void draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader, bool debug = false) const;
		void setCamera(Camera* camera);

};
