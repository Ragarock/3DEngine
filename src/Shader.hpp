#pragma once

#include <GL/glew.h>
#include <string>

#include <iostream>
#include <fstream>

#include <sstream>

#include <unordered_map>
	
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Renderer.hpp"
#include "Log.hpp"

class Shader {
	private:
		unsigned int id;
		unsigned int vid, fid;

		std::unordered_map<const char*, int> uniformLocationCache;

		const char* path;
		std::string vsource;
		std::string fsource;

		void loadSource();
		unsigned int compileShader(unsigned int type, const std::string source);

		int getUniformLocation(const char* name);

	public:
		Shader(const char* path);
		~Shader();

		void bind() const;
		void unbind() const;

		void setUniform1i(const char* name, int value);
		void setUniform1f(const char* name, float value);
		void setUniform2f(const char* name, float x, float y);
		void setUniform3f(const char* name, float x, float y, float z);
		void setUniform4f(const char* name, float x, float y, float z, float w);
		void setUniformMat4(const char* name, glm::mat4 value);
};
