#pragma once

#include <map>
#include <string>
#include <glm/glm.hpp>

#include "Shader.hpp"

struct Character {
	unsigned int id;
	glm::ivec2 size;
	glm::ivec2 bearing;
	unsigned int advance;
};

class Font {
	private:
		unsigned int quality;
		std::map<char, Character> characters;
		unsigned int vao, vbo;

	public:
		Font(const char* path, unsigned int quality);
		void render(Shader* shader, std::string text, float x, float y, float scale, glm::vec3 colour, bool debug = false);
};
