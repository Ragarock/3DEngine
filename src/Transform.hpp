#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Transform {
	private:
		glm::vec3 position;
		glm::quat rotation;
		glm::vec3 scale;

	public:
		Transform();
		~Transform();

		inline glm::mat4 getTransformation() {
			glm::mat4 result = glm::mat4();

			result =  glm::translate(result, position);
			result *= glm::toMat4(rotation);
			result =  glm::scale(result, scale);

			return result;
		}

		inline glm::vec3 getPosition() const { return position; }
		inline glm::quat getRotation() const { return rotation; }
		inline glm::vec3 getScale   () const { return scale;    }

		inline void setPosition(glm::vec3 position) { this->position = position; }
		inline void setRotation(glm::quat rotation) { this->rotation = rotation; }
		inline void setScale   (glm::vec3 scale   ) { this->scale    = scale;    }

		inline void addPosition(glm::vec3 position) { this->position += position; }
};
