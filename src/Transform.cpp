#include "Transform.hpp"

Transform::Transform() {
	position = glm::vec3(0, 0, 0);
	rotation = glm::quat(0, 0, 0, 1);
	scale    = glm::vec3(1, 1, 1);
}

Transform::~Transform() {
}
