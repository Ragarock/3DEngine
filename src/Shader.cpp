#include "Shader.hpp"

Shader::Shader(const char* path) {
	this->path = path;

	id = glCreateProgram();

	loadSource();

	vid = compileShader(GL_VERTEX_SHADER, vsource);
	fid = compileShader(GL_FRAGMENT_SHADER, fsource);

	GLCall(glAttachShader(id, vid));
	GLCall(glAttachShader(id, fid));

	// GLCall(glBindAttribLocation(id, 0, "position"));
	// GLCall(glBindAttribLocation(id, 1, "uv"));

	GLCall(glLinkProgram(id));

	int res;
	GLCall(glGetProgramiv(id, GL_LINK_STATUS, &res));
	if(res == GL_FALSE) {
		int length;
		GLCall(glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length));
		char* message = (char*)alloca(length * sizeof(char));
		GLCall(glGetProgramInfoLog(id, length, &length, message));
		Log::err("Failed to link program:");
		Log::err(message);
	}

	GLCall(glValidateProgram(id));

	GLCall(glGetProgramiv(id, GL_VALIDATE_STATUS, &res));
	if(res == GL_FALSE) {
		int length;
		GLCall(glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length));
		char* message = (char*)alloca(length * sizeof(char));
		GLCall(glGetProgramInfoLog(id, length, &length, message));
		Log::err("Failed to validate program:");
		Log::err(message);
	}
}

Shader::~Shader() {

}

void Shader::loadSource() {
	std::ifstream stream(path);

	enum class ShaderType {
		NONE = -1, VERTEX = 0, FRAGMENT = 1,
	};

	std::stringstream ss[2];
	ShaderType type = ShaderType::NONE;

	std::string line;
	while(getline(stream, line)) {
		if(line.find("#shader") != std::string::npos) {
			if(line.find("vertex") != std::string::npos) {
				// set mode to vertex
				type = ShaderType::VERTEX;
			} else if(line.find("fragment") != std::string::npos) {
				// set mode to fragment
				type = ShaderType::FRAGMENT;
			}
		} else {
			if(type != ShaderType::NONE) {
				ss[(int)type] << line << "\n";
			}
		}
	}

	vsource = ss[0].str();
	fsource = ss[1].str();
}

unsigned int Shader::compileShader(unsigned int type, const std::string source) {
	unsigned int result = glCreateShader(type);
	const char* src = source.c_str();

	GLCall(glShaderSource(result, 1, &src, nullptr));
	GLCall(glCompileShader(result));

	int res;
	GLCall(glGetShaderiv(result, GL_COMPILE_STATUS, &res));
	if(res == GL_FALSE) {
		int length;
		GLCall(glGetShaderiv(result, GL_INFO_LOG_LENGTH, &length));
		char* message = (char*)alloca(length * sizeof(char));
		GLCall(glGetShaderInfoLog(result, length, &length, message));
		if(type == GL_VERTEX_SHADER)
			Log::err("Failed to compile vertex shader:");
		else
			Log::err("Failed to compile fragment shader:");
		Log::err(message);
		GLCall(glDeleteShader(result));
		return 0;
	}

	return result;
}

void Shader::bind() const {
	GLCall(glUseProgram(id));
}

void Shader::unbind() const {
	GLCall(glUseProgram(0));
}

void Shader::setUniform1i(const char* name, int value) {
	glUniform1i(getUniformLocation(name), value);
}

void Shader::setUniform1f(const char* name, float value) {
	glUniform1f(getUniformLocation(name), value);
}

void Shader::setUniform2f(const char* name, float x, float y) {
	glUniform2f(getUniformLocation(name), x, y);
}

void Shader::setUniform3f(const char* name, float x, float y, float z) {
	glUniform3f(getUniformLocation(name), x, y, z);
}

void Shader::setUniform4f(const char* name, float x, float y, float z, float w) {
	glUniform4f(getUniformLocation(name), x, y, z, w);
}

void Shader::setUniformMat4(const char* name, glm::mat4 value) {
	glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(value));
}

int Shader::getUniformLocation(const char* name) {
	if(uniformLocationCache.find(name) != uniformLocationCache.end())
		return uniformLocationCache[name];

	GLCall(int location = glGetUniformLocation(id, name));
	if(location == -1)
		std::cout << "uniform: " << name << " doesnt exist!" << std::endl;
	uniformLocationCache[name] = location;

	return location;
}
