#pragma once

#include <vector>
#include <GL/glew.h>

#include "Renderer.hpp"

struct VertexBufferElement {
	unsigned int type;
	unsigned int count;
	unsigned char normalized;

	static unsigned int getSizeOfType(unsigned int type) {
		switch (type) {
			case GL_FLOAT:         return 4;
			case GL_UNSIGNED_INT:  return 4;
			case GL_UNSIGNED_BYTE: return 1;
		}
		ASSERT(false);
		return 0;
	}
	
};

enum class valueType {
	FLOAT=0, UNSIGNED_INT=1, UNSIGNED_BYTE=2,
};

struct VertexBufferLayout {
	private:
		std::vector<VertexBufferElement> elements;
		unsigned int stride;

	public:
		VertexBufferLayout(): stride(0) {

		}

		void push(unsigned int count, valueType value) {
			switch(value) {
				case valueType::FLOAT:
					elements.push_back({GL_FLOAT, count, GL_FALSE});
					stride += count * VertexBufferElement::getSizeOfType(GL_FLOAT);
					break;
				case valueType::UNSIGNED_INT:
					elements.push_back({GL_UNSIGNED_INT, count, GL_FALSE});
					stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_INT);
					break;
				case valueType::UNSIGNED_BYTE:
					elements.push_back({GL_UNSIGNED_BYTE, count, GL_TRUE});
					stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE);
					break;
			}
		}
		
		inline const std::vector<VertexBufferElement>& getElements() const { return elements; }
		inline unsigned int getStride() const { return stride; }

};
