#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"
#include "VertexArray.hpp"

#include "VertexBufferLayout.hpp"

#include "Renderer.hpp"

VertexArray::VertexArray() {
	GLCall(glGenVertexArrays(1, &id));
}

VertexArray::~VertexArray() {
	GLCall(glDeleteVertexArrays(1, &id));
}

void VertexArray::addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout) {
	bind();
	vb.bind();
	
	const auto& elements = layout.getElements();
	unsigned int offset = 0;
	for(unsigned int i = 0; i < elements.size(); i++) {
		const auto& element = elements[i];
		GLCall(glEnableVertexAttribArray(i));
		GLCall(glVertexAttribPointer(i, element.count, element.type, element.normalized, layout.getStride(), (const void*)offset));
		offset += element.count * VertexBufferElement::getSizeOfType(element.type);
	}
}

void VertexArray::bind() const {
	GLCall(glBindVertexArray(id));
}

void VertexArray::unbind() const {
	GLCall(glBindVertexArray(0));
}
