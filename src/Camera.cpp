#include "Camera.hpp"

#define _USE_MATH_DEFINES
#include <math.h>

glm::vec2 prevMousePos;

float normalizeYaw(float value, float start, float end) {
	const float width = end - start;
	const float offsetValue = value - start;

	// Do magic calculation stuff to set value of value inside of start - end
	return (offsetValue - (floor(offsetValue / width) * width)) + start;
}

Camera::Camera(glm::vec3 position, glm::vec3 up, glm::vec3 forward) {
	this->position = position;
	this->up = glm::normalize(up);
	this->forward = glm::normalize(forward);
	this->right = glm::normalize(glm::cross(forward, up));
}

void Camera::update(float delta) {
	glm::vec3 front;
	front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
	front.y = sin(glm::radians(pitch));
	front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
	
	this->forward = glm::normalize(front);

	updateVectors();

	if(Input::isKeyDown(GLFW_KEY_W)) {
		position += forward * speed * delta;
	}
	if(Input::isKeyDown(GLFW_KEY_S)) {
		position -= forward * speed * delta;
	}
	if(Input::isKeyDown(GLFW_KEY_A)) {
		position -= right * speed * delta;
	}
	if(Input::isKeyDown(GLFW_KEY_D)) {
		position += right * speed * delta;
	}
	if(Input::isKeyDown(GLFW_KEY_SPACE)) {
		position += up * speed * delta;
	}
	if(Input::isKeyDown(GLFW_KEY_LEFT_SHIFT)) {
		position -= up * speed * delta;
	}

	if(Input::isKeyDown(GLFW_KEY_LEFT)) {
		// Restrict the Yaw to 0-360 so we dont lose floating point precision if we keep spinning around
		yaw = glm::mod(yaw - (360 * delta), 360.0f);
	}
	if(Input::isKeyDown(GLFW_KEY_RIGHT)) {
		// Restrict the Yaw to 0-360 so we dont lose floating point precision if we keep spinning around
		yaw = glm::mod(yaw + (360 * delta), 360.0f);
	}

	if(Input::isKeyDown(GLFW_KEY_UP)) {
		pitch += 360 * delta;
	}
	if(Input::isKeyDown(GLFW_KEY_DOWN)) {
		pitch -= 360 * delta;
	}

	if(Input::isKeyDown(GLFW_KEY_R)) {
		position = glm::vec3(0, 0, 0);
	}

	if(&prevMousePos == nullptr) {
		prevMousePos = glm::vec2(Input::getX(), Input::getY());
	}

	glm::vec2 mousePos = glm::vec2(Input::getX(), Input::getY());
	glm::vec2 mouseVel = mousePos - prevMousePos;

	pitch -= (float)(mouseVel.y * 50.0f) * delta;
	yaw   += (float)(mouseVel.x * 50.0f) * delta;

	yaw = normalizeYaw(yaw, -180, 180);

	// Restrict pitch from -75 - 75 degrees
	if(pitch > 75)
		pitch = 75;
	if(pitch < -75)
		pitch = -75;

	prevMousePos.x = mousePos.x;
	prevMousePos.y = mousePos.y;
}

void Camera::updateVectors() {
	this->up = glm::normalize(up);
	this->forward = glm::normalize(forward);
	this->right = glm::normalize(glm::cross(forward, up));
}
