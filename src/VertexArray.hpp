#pragma once

#include "VertexBuffer.hpp"

class VertexBufferLayout;

class VertexArray {
	private:
		unsigned int id;

	public:
		VertexArray();
		virtual ~VertexArray();

		void bind() const;
		void unbind() const;

		void addBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout);

};
