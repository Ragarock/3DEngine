#pragma once

#include <GLFW/glfw3.h>

#include "Log.hpp"

class Window {
	private:
		GLFWwindow* id;
		int width, height;
		const char* title;

	public:
		Window(const char* title, int width, int height);
		~Window();

		void update();
		void render();

		void setTitle(const char* title);
		int getWidth();
		int getHeight();

		void setSize(int width, int height);

		bool isClosed();

};
