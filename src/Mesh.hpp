#pragma once

#include <string>

#include "VertexArray.hpp"
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"
#include "VertexBufferLayout.hpp"

class Mesh {
	private:
		float* vertices;
		unsigned int* indices;

		VertexArray* va;
		IndexBuffer* ib;
		VertexBuffer* vb;
		VertexBufferLayout vl;

	public:
		Mesh(float* vertices, unsigned int* indices, int verticesCount, int indicesCount);
		~Mesh();

		void render(Renderer* renderer, Shader* shader);

		Mesh loadMesh(const std::string& path);
};
