#ifndef LOG_H
#define LOG_H

#include <iostream>

class Log {
	private:
		static int m_logLevel;

	public:
		static const int logLevelError = 0;
		static const int logLevelWarning = 1;
		static const int logLevelInfo = 2;

		static void info(const char* msg);
		static void warn(const char* msg);
		static void err(const char* msg);

		static inline void setLevel(int level) {
			Log::m_logLevel = level;
		}
};

#endif /* ifndef LOG_H */
