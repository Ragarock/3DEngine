#include <iostream>
#include <cstring>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
	
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <sstream>

#include "Font.hpp"
#include "Log.hpp"
#include "Window.hpp"
#include "Renderer.hpp"
#include "Input.hpp"

#include "Shader.hpp"
#include "VertexArray.hpp"
#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"
#include "VertexBufferLayout.hpp"
#include "Camera.hpp"
#include "Texture.hpp"
#include "Mesh.hpp"
#include "Transform.hpp"

#include "ecs/ecs.hpp"

struct TransformComponent : public ECSComponent<TransformComponent> {
	Transform transform;
};

struct MovementControlComponent : public ECSComponent<MovementControlComponent> {
	// array of pairs consisting of vec3 move direction and InputControl
	Array<std::pair<glm::vec3, InputControl*>> movementControls;
};

class MovementControlSystem : public BaseECSSystem {
	private:
		

	public:
		MovementControlSystem() : BaseECSSystem() {
			addComponentType(TransformComponent::ID);
			addComponentType(MovementControlComponent::ID);
		}

		virtual void updateComponents(float delta, BaseECSComponent** components) {
			TransformComponent* transform = (TransformComponent*)components[0];
			MovementControlComponent* movementControl = (MovementControlComponent*)components[1];

			for(uint32 i = 0; i < movementControl->movementControls.size(); i++) {
				glm::vec3 movement  = movementControl->movementControls[i].first;
				InputControl* input = movementControl->movementControls[i].second;
				glm::vec3 newPos = transform->transform.getPosition() + (movement * input->getAmt() * delta);
				transform->transform.setPosition(newPos);
			}
		}

};

int main(void) {
	Log::setLevel(Log::logLevelInfo);
	Log::info("Starting!");
	
	Window* window = new Window("OpenGL", 2560 / 3, 1440 / 3);

	if(glewInit() != GLEW_OK) {
		Log::err("GLEW failed to initialize!");
		exit(1);
	}

	glClearColor(1, 0, 1, 1);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glCullFace(GL_BACK);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Font gohu("res/Fonts/Gohu.ttf", 1);

	float positions[] = {
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0, 0, // 0
		 0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1, 0, // 1
		 0.5f,  0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1, 1, // 2
		-0.5f,  0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0, 1, // 3

		-0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1, 0, // 4
		 0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0, 0, // 5
		 0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0, 1, // 6
		-0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 1, 1, // 7

		-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1, 0, // 8
		 0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0, 0, // 9
		 0.5f, -0.5f,  0.5f, 0.0f, -1.0f, 0.0f, 0, 1, // 10
		-0.5f, -0.5f,  0.5f, 0.0f, -1.0f, 0.0f, 1, 1, // 11

		-0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0, 0, // 12
		 0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1, 0, // 13
		 0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 1, 1, // 14
		-0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0, 1, // 15

		-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1, 0, // 16
		-0.5f, -0.5f,  0.5f, -1.0f, 0.0f, 0.0f, 0, 0, // 17
		-0.5f,  0.5f,  0.5f, -1.0f, 0.0f, 0.0f, 0, 1, // 18
		-0.5f,  0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1, 1, // 19

		 0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0, 0, // 20
		 0.5f, -0.5f,  0.5f, 1.0f, 0.0f, 0.0f, 1, 0, // 21
		 0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f, 1, 1, // 22
		 0.5f,  0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0, 1, // 23
	};

	unsigned int indices[] = {
		2, 1, 0,
		0, 3, 2,

		4, 5, 6,
		6, 7, 4,

		8, 9, 10,
		10, 11, 8,

		14, 13, 12,
		12, 15, 14,

		16, 17, 18,
		18, 19, 16,

		22, 21, 20,
		20, 23, 22,
	};

	unsigned int indices2[] = {
		0, 1, 2,
		2, 3, 0,

		6, 5, 4,
		4, 7, 6,

		10, 9, 8,
		8, 11, 10,

		12, 13, 14,
		14, 15, 12,

		18, 17, 16,
		16, 19, 18,

		20, 21, 22,
		22, 23, 20,
	};

	Renderer* renderer = new Renderer();
	Camera* camera = new Camera(glm::vec3(0, 0, 0));
	renderer->setCamera(camera);

	Texture texture("res/Textures/lain.png", GL_TEXTURE_2D);
	texture.bind();
	
	Mesh mesh(positions, indices2, sizeof(positions) / sizeof(float), sizeof(indices2) / sizeof(unsigned int));
	Mesh mesh2(positions, indices, sizeof(positions) / sizeof(float), sizeof(indices) / sizeof(unsigned int));

	Shader* shader = new Shader("res/Shaders/Basic.glsl");
	Shader* fontShader = new Shader("res/Shaders/Font.glsl");

	// glm::mat4 view;
	// view = glm::lookAt(glm::vec3(0, 1, -1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	// glm::mat4 projection = glm::ortho(0.0f, (float)window->getWidth(), 0.0f, (float)window->getHeight(), -100.0f, 100.0f);
	glm::mat4 projection = glm::perspective(glm::radians(90.0f), (float)window->getWidth() / (float)window->getHeight(), 0.01f, 1000.0f);

	glm::mat4 model;
	model = glm::translate(model, glm::vec3(0, 0, 0));
	// trans = glm::rotate(trans, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));

	// Input
	
	InputControl horInputControl;
	InputControl verInputControl;

	Input::addKeyControl(GLFW_KEY_L, horInputControl, 1.0f);
	Input::addKeyControl(GLFW_KEY_J, horInputControl, -1.0f);

	Input::addKeyControl(GLFW_KEY_I, verInputControl, 1.0f);
	Input::addKeyControl(GLFW_KEY_K, verInputControl, -1.0f);

	// ECS TESTING
	
	ECS ecs;

	// components
	
	TransformComponent transformComponent;
	transformComponent.transform.setPosition(glm::vec3(0.0f, 0.0f, 0.0f));

	MovementControlComponent movementControl;
	movementControl.movementControls.push_back(std::make_pair(glm::vec3(0.0f, 0.0f, 1.0f) * 16.0f, &horInputControl));
	movementControl.movementControls.push_back(std::make_pair(glm::vec3(0.0f, 1.0f, 0.0f) * 16.0f, &verInputControl));

	// Entities
	
	EntityHandle entity = ecs.makeEntity(transformComponent, movementControl);
	
	// Systems

	MovementControlSystem movementControlSystem;
	ECSSystemList mainSystems;
	mainSystems.addSystem(movementControlSystem);
	
	// END ECS TESTING

	bool running = true;
	float now = glfwGetTime();

	float lastTime = now;
	float delta = 0;
	int fps = 0;
	int lastFPS = 0;

	float timer = 0;

	bool debug = false;

	while(running) {
		now = glfwGetTime();
		delta = now - lastTime;
		timer += delta;

		if(timer > 1) {
			timer -= 1;
			std::cout << "FPS: " << fps << std::endl;
			lastFPS = fps;
			fps = 0;
		}
		fps++;

		if(Input::isKeyPressed(GLFW_KEY_ESCAPE))
			running = false;
	
		glm::mat4 projection = glm::perspective(glm::radians(90.0f), (float)window->getWidth() / (float)window->getHeight(), 0.01f, 100.0f);
		// trans = glm::rotate(trans, glm::radians(90.0f * delta), glm::vec3(0.0f, 0.0f, 1.0f));
		// model = glm::rotate(model, glm::radians(90.0f * delta), glm::vec3(1, 1, 1));

		camera->update(delta);

		ecs.updateSystems(mainSystems, delta);

		Transform& transformation = ecs.getComponent<TransformComponent>(entity)->transform;

		// transformation.addPosition(glm::vec3(0, verBoxControl.getAmt() * 10.0f * delta, horBoxControl.getAmt() * 10.0f * delta));

		renderer->clear();
		
		window->update();

		if(Input::isKeyPressed(GLFW_KEY_E)) {
			debug = !debug;
		}

		// SKYBOX
		/*
		glDisable(GL_DEPTH_TEST);
		shader->bind();
		shader->setUniformMat4("model", glm::translate(glm::mat4(), glm::vec3(0, 0, 0)));
		shader->setUniformMat4("projection", projection);
		shader->setUniformMat4("view", camera->getSkyboxView());
		texture.bind();
		mesh.render(renderer, shader);
		glEnable(GL_DEPTH_TEST);
		*/

		// Box
		shader->bind();
		// Transform& transformation = ecs.getComponent<TransformComponent>(entity)->transform;
		shader->setUniformMat4("model", transformation.getTransformation());
		// shader->setUniformMat4("model", )
		// shader->setUniformMat4("model", model);
		shader->setUniformMat4("projection", projection);
		shader->setUniformMat4("view", camera->getView());
		texture.bind();
		mesh2.render(renderer, shader);

		// GUI TEXT
		glDisable(GL_DEPTH_TEST);
		fontShader->bind();
		// projection = glm::ortho(0.0f, (float)window->getWidth(), 0.0f, (float)window->getHeight(), -1.0f, 1.0f);
		fontShader->setUniformMat4("view", glm::translate(glm::mat4(), glm::vec3(0, 0, 0)));
		// fontShader->setUniformMat4("view", glm::translate(glm::mat4(), glm::vec3(0, 0, 0)));
		fontShader->setUniformMat4("projection", projection);
		glm::mat4 mod = glm::translate(glm::mat4(), glm::vec3(-8, -4, -5));
		mod = glm::rotate(mod, glm::radians(22.5f), glm::vec3(0, 1, 0));
		fontShader->setUniformMat4("model", mod);
		std::ostringstream oss;
		oss << "FPS: [" << lastFPS << "]";
		std::ostringstream pos;
		pos << "POS: [" << camera->position.x << ", " << camera->position.y << ", " << camera->position.z << "]";
		std::ostringstream mps;
		mps << "POS: [" << camera->pitch << ", " << camera->yaw << "]";
		gohu.render(fontShader, pos.str(), 0, 0, 0.25, glm::vec3(0, 1, 0));
		gohu.render(fontShader, mps.str(), 0, 1, 0.25, glm::vec3(0, 1, 0));
		gohu.render(fontShader, oss.str(), 0, 2, 0.25, glm::vec3(0, 1, 0));
		glEnable(GL_DEPTH_TEST);

		window->render();

		if(window->isClosed())
			running = false;

		lastTime = now;
	}

	delete window;

	Log::info("Stopping!");
	return 0;
}
