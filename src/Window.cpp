#include "Window.hpp"

#include "Input.hpp"

void framebufferSizeCallback(GLFWwindow* window, int width, int height);

Window::Window(const char* title, int width, int height) {
	this->title = title;
	this->width = width;
	this->height = height;
	
	if(!glfwInit()) {
		Log::err("GLFW failed to initialize!");
		exit(1);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	id = glfwCreateWindow(width, height, title, nullptr, nullptr);

	glfwSetFramebufferSizeCallback(id, framebufferSizeCallback);
	glfwSetKeyCallback(id, Input::keyCallback);
	glfwSetMouseButtonCallback(id, Input::mouseButtonCallback);
	glfwSetCursorPosCallback(id, Input::cursorPosCallback);
	glfwSetWindowUserPointer(id, this);

	if(id == NULL) {
		Log::err("GLFW failed to create window!");
		exit(1);
	}

	glfwMakeContextCurrent(id);
	glViewport(0, 0, width, height);

	glfwSetInputMode(id, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

Window::~Window() {
	glfwTerminate();
}

void Window::update() {
	Input::update();
	glfwPollEvents();
}

void Window::render() {
	glfwSwapBuffers(id);
}

void Window::setTitle(const char* title) {
	this->title = title;
	glfwSetWindowTitle(id, title);
}

int Window::getWidth() {
	return width;
}

int Window::getHeight() {
	return height;
}

void Window::setSize(int width, int height) {
	if(this->width != width && this->height != height) {
		this->width = width;
		this->height = height;
		glViewport(0, 0, width, height);
	}
}

bool Window::isClosed() {
	return glfwWindowShouldClose(id);
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	((Window*)glfwGetWindowUserPointer(window))->setSize(width, height);
}
