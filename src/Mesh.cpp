#include "Mesh.hpp"

#include <vector>

#include "Texture.hpp"

Mesh::Mesh(float* vertices, unsigned int* indices, int verticesCount, int indicesCount) {
	this->vertices = vertices;
	this->indices = indices;

	va = new VertexArray();
	vb = new VertexBuffer(vertices, verticesCount * sizeof(float));

	// create the Layout of the vertexArray buffers
	vl.push(3, valueType::FLOAT);
	vl.push(3, valueType::FLOAT);
	vl.push(2, valueType::FLOAT);
	va->addBuffer(*vb, vl);

	ib = new IndexBuffer(indices, indicesCount);
}

Mesh::~Mesh() {
}

void Mesh::render(Renderer* renderer, Shader* shader) {
	renderer->draw(*va, *ib, *shader, false);
}

Mesh Mesh::loadMesh(const std::string& path) {
	// std::vector<float> vertices;
	// std::vector<unsigned int> indices;
	// std::vector<Texture> textures;
	

	Mesh result(nullptr, nullptr, 0, 0);
	return result;
}
