#include "Input.hpp"

#define MOUSE_OFFSET 1024

Input::Input() {
	std::cout << "Input created!" << std::endl;
	for(int i = 0; i < GLFW_KEY_LAST; i++) {
		prevKeys[i] = false;
		currKeys[i] = false;
		if(i < GLFW_MOUSE_BUTTON_LAST) {
			prevMouseButtons[i] = false;
			currMouseButtons[i] = false;
		}
	}
}

Input::~Input() {

}

void Input::update() {
	for(int i = 0; i < GLFW_KEY_LAST; i++) {
		Input::getInstance()->prevKeys[i] = Input::getInstance()->currKeys[i];

		if(i < GLFW_MOUSE_BUTTON_LAST) {
			Input::getInstance()->prevMouseButtons[i] = Input::getInstance()->currMouseButtons[i];
		}
	}
}

void Input::updateInput(uint32 inputCode, float dir) {
	for(uint32 i = 0; i < Input::getInstance()->inputs[inputCode].size(); i++) {
		Input::getInstance()->inputs[inputCode][i].second.addAmt(Input::getInstance()->inputs[inputCode][i].first * dir);
	}
}

void Input::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	Input::getInstance()->currKeys[key] = (action != GLFW_RELEASE);

	// if pressed
	if(action == GLFW_PRESS && !Input::getInstance()->prevKeys[key])
		updateInput(key, 1.0f);

	// if released
	if(action == GLFW_RELEASE && Input::getInstance()->prevKeys[key])
		updateInput(key, -1.0f);
}

void Input::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
	Input::getInstance()->currMouseButtons[button] = (action != GLFW_RELEASE);
	
	// if pressed
	if(action == GLFW_PRESS && !Input::getInstance()->prevMouseButtons[button])
		updateInput(button + MOUSE_OFFSET, 1.0f);

	// if released
	if(action == GLFW_RELEASE && Input::getInstance()->prevMouseButtons[button])
		updateInput(button + MOUSE_OFFSET, -1.0f);
}

void Input::cursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
	Input::getInstance()->x = (float)xpos;
	Input::getInstance()->y = (float)ypos;
}

void Input::addKeyControl(int key, InputControl& inputControl, float weight) {
	Input::getInstance()->inputs[key].push_back(std::pair<float, InputControl&>(weight, inputControl));
}

void Input::addMouseControl(int button, InputControl& inputControl, float weight) {
	Input::getInstance()->inputs[button + MOUSE_OFFSET].push_back(std::pair<float, InputControl&>(weight, inputControl));
}

bool Input::isKeyDown(int key) {
	return Input::getInstance()->currKeys[key];
}

bool Input::isKeyPressed(int key) {
	return Input::getInstance()->currKeys[key] && !Input::getInstance()->prevKeys[key];
}

bool Input::isKeyReleased(int key) {
	return !Input::getInstance()->currKeys[key] && Input::getInstance()->prevKeys[key];
}

bool Input::isMouseButtonDown(int button) {
	return Input::getInstance()->currMouseButtons[button];
}

bool Input::isMouseButtonPressed(int button) {
	return Input::getInstance()->currMouseButtons[button] && !Input::getInstance()->prevMouseButtons[button];
}

bool Input::isMouseButtonReleased(int button) {
	return !Input::getInstance()->currMouseButtons[button] && Input::getInstance()->prevMouseButtons[button];
}
