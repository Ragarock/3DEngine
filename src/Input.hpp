#pragma once

#include <iostream>
#include <GLFW/glfw3.h>

#include "dataStructures/map.hpp"
#include "dataStructures/array.hpp"

#include "InputControl.hpp"

class Input {
	public:
		Map<uint32, Array<std::pair<float, InputControl&>>> inputs;

	private:
		Input();
		~Input();

		bool currKeys[GLFW_KEY_LAST];
		bool prevKeys[GLFW_KEY_LAST];

		bool currMouseButtons[GLFW_MOUSE_BUTTON_LAST];
		bool prevMouseButtons[GLFW_MOUSE_BUTTON_LAST]; 
		float x, y;

	public:
		static Input* getInstance() {
			static Input* instance;
			if(!instance)
				instance = new Input();
			return instance;
		}

		static void update();
		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void cursorPosCallback(GLFWwindow* window, double xpos, double ypos);
		static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static bool isKeyDown(int key);
		static bool isKeyPressed(int key);
		static bool isKeyReleased(int key);
		static bool isMouseButtonDown(int button);
		static bool isMouseButtonPressed(int button);
		static bool isMouseButtonReleased(int button);

		static void addKeyControl(int key, InputControl& inputControl, float weight = 1.0f);
		static void addMouseControl(int button, InputControl& inputControl, float weight = 1.0f);

		static void updateInput(uint32 inputCode, float dir);

		static float getX() { return getInstance()->x; }
		static float getY() { return getInstance()->y; }
};
