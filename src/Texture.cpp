#include "Texture.hpp"

#include "Vendor/stb_image.h"

Texture::Texture(const std::string& path, int type) {
	this->id = 0;
	this->path = path;
	this->localBuffer = nullptr;
	this->width = 0;
	this->height = 0;
	this->bpp = 0;

	switch(type) {
		case GL_TEXTURE_2D:
			loadTexture();
			break;
		case GL_TEXTURE_CUBE_MAP:
			loadCubemap();
			break;
	}
}

Texture::~Texture() {
	GLCall(glDeleteTextures(1, &id));
}

void Texture::bind(unsigned int index) const {
	GLCall(glActiveTexture(GL_TEXTURE0 + index));
	GLCall(glBindTexture(GL_TEXTURE_2D, id));
}

void Texture::unbind() const {
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
}

void Texture::loadTexture() {
	stbi_set_flip_vertically_on_load(1);
	localBuffer = stbi_load(path.c_str(), &width, &height, &bpp, 4);

	GLCall(glGenTextures(1, &id));
	GLCall(glBindTexture(GL_TEXTURE_2D, id));

	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));

	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, localBuffer));
	
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
	if(localBuffer)
		stbi_image_free(localBuffer);
}

void Texture::loadCubemap() {
	
}
