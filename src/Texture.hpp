#pragma once

#include "Renderer.hpp"

class Texture {
	private:
		unsigned int id;
		std::string path;
		unsigned char* localBuffer;
		int width, height, bpp;
		int type;

	public:
		Texture(const std::string& path, int type);
		~Texture();

		void bind(unsigned int index = 0) const;
		void unbind() const;

		inline int getWidth() const { return width; }
		inline int getHeight() const { return height; }

		void loadTexture();
		void loadCubemap();
};
