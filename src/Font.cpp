#include "Font.hpp"

#include <GL/glew.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>

Font::Font(const char* path, unsigned int quality) {
	FT_Library ft;
	if(FT_Init_FreeType(&ft))
		std::cout << "ERROR::FREETYPE: could not init freetype library" << std::endl;
	FT_Face face;
	if(FT_New_Face(ft, path, 0, &face))
		std::cout << "ERROR::FREETYPE: failed to load font" << std::endl;

	this->quality = quality;
	// Pixel size is set to 1024 to combat the AntiAliasing Freetype forces on the user
	FT_Set_Pixel_Sizes(face, 0, 2048 / quality);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for(unsigned char c = 0; c < 127; c++) {
		if(FT_Load_Char(face, c, FT_LOAD_RENDER)) {
			std::cout << "ERROR::FREETYPE: Failed to load glyph" << std::endl;
			continue;
		}

		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_RED,
				face->glyph->bitmap.width,
				face->glyph->bitmap.rows,
				0,
				GL_RED,
				GL_UNSIGNED_BYTE,
				face->glyph->bitmap.buffer
				);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		Character character = {
			texture,
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			(unsigned int)face->glyph->advance.x
		};

		characters.insert(std::pair<char, Character>(c, character));
	}

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Font::render(Shader* shader, std::string text, float x, float y, float scale, glm::vec3 colour, bool debug) {
	shader->bind();
	shader->setUniform3f("texColour", colour.x, colour.y, colour.z);
	shader->setUniform1i("sampler", 0);
	shader->setUniform1i("debug", debug? 1:0);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(vao);

	// divide the scale of the font by 1024 so that the scale the user passes us equals the height in pixels
	scale /= 2048 / quality;

	std::string::const_iterator c;
	for(c = text.begin(); c != text.end(); c++) {
		Character ch = characters[*c];
		float xp = x + ch.bearing.x * scale;
		float yp = y - (ch.size.y - ch.bearing.y) * scale;

		float w = ch.size.x * scale;
		float h = ch.size.y * scale;

		float vertices[6][4] = {
			{xp    , yp + h, 0, 0},
			{xp    , yp    , 0, 1},
			{xp + w, yp    , 1, 1},
			
			{xp    , yp + h, 0, 0},
			{xp + w, yp    , 1, 1},
			{xp + w, yp + h, 1, 0},
		};

		glBindTexture(GL_TEXTURE_2D, ch.id);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);
		x += (ch.advance >> 6) * scale;
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}
