#shader vertex
#version 330 core

attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;

varying vec3 out_pos;
varying vec3 out_nor;
varying vec2 out_uv;

uniform mat4 view = mat4(1);
uniform mat4 projection = mat4(1);
uniform mat4 model = mat4(1);

void main() {
	out_uv = uv;
	out_nor = normal;
	out_pos = position;
	gl_Position = projection * view * model * vec4(position, 1);
}

#shader fragment
#version 330 core

varying vec3 out_pos;
varying vec3 out_nor;
varying vec2 out_uv;

out vec4 colour;

uniform float red;
uniform sampler2D sampler;

void main() {
	vec4 tex = texture2D(sampler, out_uv);
	// colour = vec4(abs(out_pos), 1);
	colour = tex;
	// colour = tex * vec4(abs(out_pos), 1);
}
