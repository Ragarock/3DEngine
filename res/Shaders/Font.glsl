#shader vertex
#version 330 core

attribute vec4 vertex;

varying vec2 uv;

uniform mat4 view = mat4(1);
uniform mat4 projection = mat4(1);
uniform mat4 model = mat4(1);

void main() {
	uv = vertex.zw;
	gl_Position = projection * view * model * vec4(vertex.xy, 0, 1);
}

#shader fragment
#version 330 core

in vec2 uv;

out vec4 colour;

uniform sampler2D sampler;
uniform vec3 texColour = vec3(0, 0, 0);
uniform int debug = 0;

void main() {
	float tex = texture(sampler, uv).x;
	tex = sign(tex) * floor(abs(tex) + 0.5);
	if(tex == 0 && debug == 1) {
		colour = vec4(1, 1, 1, 1);
	} else {
		colour = vec4(texColour, tex);
	}
}
