BUILD_TARGET="Release"

if [ "$#" == 0 ]; then
	BUILD_TARGET="Release"
else
	BUILD_TARGET="$1"
fi

mkdir Build
cd Build
cmake -DCMAKE_BUILD_TYPE="$BUILD_TARGET" "{@:1}" ../
make -j9
